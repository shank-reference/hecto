use std::io::{self, Read, Write};
use thiserror::Error;

type Result<T, E = HectoError> = std::result::Result<T, E>;

fn main() -> Result<()> {
    crossterm::terminal::enable_raw_mode()?;

    let mut stdout = io::stdout();
    let stdin = io::stdin().bytes();

    for b in stdin {
        let b = b?;

        if b == 'q' as u8 & 0b0001_1111 {
            // ctrl+q
            break;
        }

        let c = b as char;
        if c.is_control() {
            write!(stdout, "{:?}\r\n", &b)?;
        } else {
            write!(stdout, "{:?}: ({})\r\n", &b, &c)?;
        }
        stdout.flush()?;
    }

    crossterm::terminal::disable_raw_mode()?;

    Ok(())
}

#[derive(Debug, Error)]
enum HectoError {
    #[error(transparent)]
    CrosstermError(#[from] crossterm::ErrorKind),

    #[error(transparent)]
    IoError(#[from] io::Error),
}
